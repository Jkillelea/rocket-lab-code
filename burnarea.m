function [A, V] = burnarea(r, h, rb)
% r: propellant grain radius
% h: propellant grain height
% rb: propellant grain burn radius
    fprintf('rb=%.8f r=%.8f h=%.8f\n', rb, r, h);

    % Burnout
    if (rb >= r) || (rb >= h)
        A = 0;
        V = pi*r^2*h;
    end

    % Length of a slot side
    L = sqrt(r^2 - rb^2);
    theta = asin(rb / r);

    % Base of the cylinder
    Abase = pi*r^2 - 0.5*pi*rb^2 - rb*L - 0.5*theta*r^2;

    % Burning slot
    Aslot = (pi*rb + 2*L)*(h - rb);

    % Total burning area
    A = Abase + Aslot;

    % Cavity volume
    V = pi*rb*r^2 + (pi*r^2 - Abase)*(h - rb);
end


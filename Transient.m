close all;clear all;clc;

t(1)  = 0; % [s] initial time
rb(1) = 0; % [m] initial burn grain displacement
T_claimed = csvread('Thrust.csv');

%% INPUTS
cstar_eff = 0.9;       % [-]     cstar efficiency
t_step    = 0.005;       % [s]     time step
P_atm     = 101325;    % [Pa]  ambient pressure
%P_atm     = 101.325;   % [kPa]  ambient pressure
%P_atm     = 14.696;    % [psia]  ambient pressure
%a         = 0.000005;  % [-]     burn rate coefficient
a         = 0.000005;
n         = 0.53;       % [-]     burn rate exponent
%.45 gets us 3.5 seconds with a = 0.000005
cstar     = 1500;      % [m/s]   characteristic velocity
h_grain   = 1.915;     % [in]    motor grain height
r_grain   = 0.435;     % [in]    motor grain radius
r_throat  = 0.0615;    % [in]    throat radius
r_exit    = 0.1155;    % [in]    exit radius
Mass      = 0.030;     % [kg]

%% CONVERSIONS
h_grain  = h_grain*0.0254;  % [m] motor grain height
r_grain  = r_grain*0.0254;  % [m] motor grain radius
r_throat = r_throat*0.0254; % [m] throat radius
r_exit   = r_exit*0.0254;   % [m] exit radius
a        = a*0.0254;        % [m] burn rate coeff

%% QUANTITY CALCULATIONS
Vol      = h_grain*r_grain^2*pi(); % [m^3]
rho_p    = Mass/Vol;               % [kg/m^3]
A_throat = pi()*(r_throat)^2;      % [m^2]
A_exit   = pi()*(r_exit)^2;        % [m^2]
AR_sup   = A_exit/A_throat;        % supersonic area ratio

V_burn = [0]; % [m^3]
j = 1;
while rb < r_grain && rb < h_grain % while there is unburned grain remaining
    [A_burn(j), V_burn(j+1)] = burnarea(r_grain, h_grain, rb); % [m^2] burn area, [m^3] burn cavity volume

    Pc(j) = (((a * rho_p * A_burn(j) * cstar) / A_throat).^(1/(1-n)))/1e6; % [MPa] chamber pressure

    burn_rate(j) = a*(Pc(j)*1e6)^n; % [m/s] burn rate

    rb = rb + burn_rate(j) * t_step; % [m] updates burn displacement
    
    delta_Vol = V_burn(j+1) - V_burn(j); % [m^3/s] rate of change in burn cavity volume 

    [T_predicted(j), cstar] = thrust_calc(P_atm, Pc(j), A_exit, rho_p, burn_rate(j), A_burn(j), AR_sup); % , delta_Vol);

    cstar = cstar*cstar_eff; % [m/s]
    if j == 1
        t(j) = t_step;
    else
        t(j) = t(j-1) + t_step;
    end
    j = j+1;
end
figure;hold on;
subplot(1, 2, 1);
plot(T_claimed(:,1), T_claimed(:,2));
xlabel('Time');
ylabel('Thrust');
title('Reported Thrust');
% figure; hold on;
subplot(1, 2, 2);
plot(t, T_predicted);
xlabel('Time');
ylabel('Thrust');
title('Calculated Thrust');

function [Ab, Vb] = burn_geometry_M(r, h, rb)
    % Inputs:
    %   r:  propellant grain radius
    %   h:  propellant grain height
    %   rb: propellant linear ablation 
    %       (also the radius of the inside slot)
    fprintf('rb=%.8f r=%.8f h=%.8f\n', rb, r, h);
    if rb >= r % motor is burnt out
        Ab = 0; % [m^2] 
        Vb = 0; % [m^3] 
    else % there is grain remaining
        %% BURN AREA
        % Burn area for rocket lab
        % Semicircle at top
        A1 = (1/2)*pi*(rb^2);

        % Middle triangle
        L = sqrt((rb^2) + (r^2));
        A2 = 2*rb*L;

        % Remaining Segment
        theta = asin(rb/r);
        A6    = (1/2)*(r^2)*theta; % pi slice
        A5    = L*rb;         % triangle
        A3    = A6-A5;              % segment is different

        % Burnt area is sum of parts
        Aburnt = A1+A2+A3;

        % Area remaining to be burnt is difference from total circle
        AH = (pi*(r^2))-Aburnt;

        % Vertical area is perimeter of horiontal area
        % Time height remaining to be burnt
        % Burnt height = rb assuming equal burn in all direction
        % Height remaining = hg-rb
        AV = (h-rb)*(2*L+pi*rb);

        % Burn area sum of parts
        Ab = AH + AV;

        %% BURN VOLUME
        Vb = Ab*(h-rb); % [m^3]
    end
end

function [mdotp] = massflow(rho_p, rho_g, burn_rate, A_burn)
    % propellant mass flow rate in terms of input to thrust_calc
    mdotp = (rho_p - rho_g)*A_burn*burn_rate;
end


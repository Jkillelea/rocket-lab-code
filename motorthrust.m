function [F] = motorthrust(Pa, Pe, ~, Ae, ~, ~, ~, ~, m_dot, Ve)
    % propellant mass flow rate in terms of input to thrust_calc
    % Pa    [Pa]
    % Pe    [Pa]
    % Ae    [m^2] 
    % m_dot [kg/s]
    % Ve    [m/s]
    gc = 1;
    F = m_dot*Ve/gc + (Pe - Pa)*Ae;
end


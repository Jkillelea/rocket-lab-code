function [Ab, Vb] = burn_geometry(r, h, rb)
    % Inputs:
    %   r:  propellant grain radius
    %   h:  propellant grain height
    %   rb: propellant linear ablation 
    %       (also the radius of the inside slot)
    fprintf('rb=%.8f r=%.8f h=%.8f\n', rb, r, h);
    
    if rb >= r % motor is burnt out
        Ab = 0; % [m^2] 
        Vb = 0; % [m^3] 
    else % there is grain remaining
        theta = asin(rb/r);
        L = sqrt((r^2)-(rb^2));
        Ac = 0.5*(r^2)*theta + rb*L + 0.5*pi*(rb^2);
        %% BURN AREA
        % Area of the end of the grain = circle area minus the 
        % area buned by the slot
        Pc = pi*rb + 2*L;
        Ab = (pi*(r^2) - Ac) + Pc*(h-rb);
        %% BURN VOLUME
        %Vb = 0;
        Vb = (h-rb)*Ac + rb*(pi*(r^2));
    end
end

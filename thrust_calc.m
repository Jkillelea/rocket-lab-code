function [ThSM_en, Cs] = thrust_calc(Pa, Pc, Ae, rho_p, burn_rate, A_burn, AR_sup) % delta_Vol)
    Pc_en = Pc * 145.038; % [psi] chamber pressure

    %% CEA RUN 
    try % tests if there is any CEA output
        % OUTPUT1 gives values in the chamber
        % OUTPUT3 gives values in nozzle exit
        [Output1, Output3] = RUN_CEA(Pc_en, AR_sup);

        alpha = Output3.a;       % [m/s]    sonic velocity
        Mach  = Output3.Mach;    % Mach     number
        Pe    = Output3.P * 1e5; % [Pa]     nozzle exit pressure
        Cs    = Output3.Cstar;   % [m/s]    characteristic velocity
        rho_g = Output1.rho;     % [kg/m^3] propellant gas density, chamber
    catch
        alpha = 0;
        Mach  = 0;
        Pe    = Pa;
        Cs    = 0;
        rho_g = 0;
    end
    
    %% MASS FLOW CALCULATION
    m_dot = massflow(rho_p, rho_g, burn_rate, A_burn); % [kg/s] propellant mass flow rate

    Ve = Mach*alpha;

    % Varaible [Units]
    % Pa    [Pa]
    % Pe    [Pa]
    % Ae    [m^2] 
    % m_dot [kg/s]
    % Ve    [m/s]
    %% THRUST CALCULATION
    ThSM = motorthrust(Pa, Pe, Pc, Ae, rho_p, burn_rate, A_burn, AR_sup, m_dot, Ve); % [N] thrust SI
    ThSM_en = ThSM * 0.224809; % [lbf] imperial thrust to match curve data
end
